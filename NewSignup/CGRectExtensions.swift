//
//  CGRectExtensions.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/16/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit


extension CGRect {
    init(center: CGPoint, size: CGSize) {
        self.size = size
        self.origin = CGPoint(x: center.x - size.width / 2, y: center.y - size.width / 2)
    }
    
    var center: CGPoint {
        return CGPoint(x: midX, y: midY)
    }
}