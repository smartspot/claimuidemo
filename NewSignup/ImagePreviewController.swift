//
//  ImagePreviewController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/15/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit


class ImagePreviewController : UIViewController {
    let image : UIImage
    let imageView : UIImageView
    
    init(image: UIImage) {
        self.image = image
        self.imageView = UIImageView(image: image)
        
        super.init(nibName: nil, bundle: nil)
        
        imageView.frame = self.view.bounds
        imageView.contentMode = .ScaleAspectFill
        self.view.addSubview(imageView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func tapReleased(sender: AnyObject) {
        
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}