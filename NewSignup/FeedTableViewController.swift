//
//  FeedTableViewController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/17/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift
import RxCocoa
import SwiftDate


public class WorkoutSetClaimGroup : SectionModelType {
    let date: NSDate
    var claims: [WorkoutSetClaim]
    public var items: [[WorkoutSetClaim]] {
        var workstationGroups = Dictionary<Workstation.IdType, [WorkoutSetClaim]>()
        
        for claim in claims {
            let key = claim.workoutSet.workstationId
            if case nil = workstationGroups[key]?.append(claim) {
                workstationGroups[key] = [claim]
            }
        }
        
        return Array(workstationGroups.values)
    }
    
    var title : String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEEE, MMMM d"
        return formatter.stringFromDate(date)
    }
    
    init(claims: [WorkoutSetClaim]) {
        self.date = claims.first!.workoutSet.startTime
        self.claims = claims
    }
    
    init(date: NSDate, claims: [WorkoutSetClaim]) {
        self.date = date
        self.claims = claims
    }

    required public init(original: WorkoutSetClaimGroup, items: [[WorkoutSetClaim]]) {
        self.date = items[0].first!.workoutSet.startTime
        self.claims = items[0]
    }
}

public class WorkoutSetClaimCell : UICollectionViewCell {
    var claim: WorkoutSetClaim!
    
    @IBOutlet weak var workoutSetClaimView: WorkoutSetClaimView!
    
    func render(claim: WorkoutSetClaim) {
        self.claim = claim
        workoutSetClaimView.claim = claim
    }
}

class WorkoutSetClaimGroupCell : UITableViewCell {
    @IBOutlet weak var mainClaim: WorkoutSetClaimView!
    @IBOutlet weak var twoSpot: WorkoutSetClaimView!
    @IBOutlet weak var threeSpot: UIImageView!
    @IBOutlet weak var plusButton: UIButton!
    
    @IBOutlet weak var containerView: UIView!
    
    var group : WorkoutSetClaimGroup!
    
    let disposeBag = DisposeBag()
    var disposableButtonBinding: Disposable?
    
    func render(group: WorkoutSetClaimGroup, index: Int, showDetail: ([WorkoutSetClaim]) -> Void) {
        containerView.layer.cornerRadius = 8.0
        let claims = group.items[index]
        
        mainClaim.claim = claims.first
        
        if group.claims.count > 1 {
            twoSpot.claim = claims[1]
        }
        
        if group.claims.count > 2 {
            let thirdClaim = claims[2]
            
            threeSpot.setUrl(thirdClaim.workoutSet.thumbnail.url)
            plusButton.setTitle("+\(claims.count - 2)", forState: .Normal)
        }
        
        // discard existing button bindings
        disposableButtonBinding?.dispose()
        // create new button bindings
        disposableButtonBinding = plusButton.rx_tap
            .subscribeNext({showDetail(group.items[index])})
    }
}

class FeedTableViewController: UITableViewController {
    let disposeBag = DisposeBag()
    let provider = WorkoutSetClaimWebApiProvider()
    let groups = Variable([WorkoutSetClaimGroup]())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        SessionProvider().login().subscribeNext({ _ in
            self.refresh()
        })
        .addDisposableTo(disposeBag)
        
        let dataSource = RxTableViewSectionedReloadDataSource<WorkoutSetClaimGroup>()
        setUpCells(dataSource)
        
        groups.asObservable()
            .bindTo(self.tableView.rx_itemsWithDataSource(dataSource))
            .addDisposableTo(disposeBag)
 
        self.tableView.rowHeight = 350
        
        initializeRefreshControl()
    }
    
    func showGroupDetail(group: WorkoutSetClaimGroup) {
        let controller = WorkstationDayDetailController.create(group.claims)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func showClaimsDetail(claims: [WorkoutSetClaim]) {
        let controller = WorkstationDayDetailController.create(claims)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func setUpCells(dataSource: RxTableViewSectionedReloadDataSource<WorkoutSetClaimGroup>) {
        dataSource.configureCell = { (dataSource, table, indexPath, _) in
            let group = self.groups.value[indexPath.section]
            let cell = table.dequeueReusableCellWithIdentifier("WorkoutSetClaimGroupCell", forIndexPath: indexPath) as! WorkoutSetClaimGroupCell
            
            cell.render(group, index: indexPath.row, showDetail: self.showClaimsDetail)
            return cell
        }
        
        dataSource.titleForHeaderInSection = { dataSource, index in
            let group = dataSource.sectionAtIndex(index)
            
            return group.title
        }
        
    }
    
    func refresh() {
        self.provider.getClaims(SessionProvider.sharedSession!)
            .doOn({_ in 
                self.refreshControl!.endRefreshing()
            })
            .subscribeNext { [unowned self] claims in
                var groups = Dictionary<NSDate, [WorkoutSetClaim]>()
                
                for claim in claims {
                    let key = claim.workoutSet.startTime.startOf(.Day)
                    if case nil = groups[key]?.append(claim) {
                        groups[key] = [claim]
                    }
                }
                
                self.groups.value = groups.values.map(WorkoutSetClaimGroup.init)
            }
            .addDisposableTo(disposeBag)
        
    }
    
    func initializeRefreshControl() {
        self.refreshControl = UIRefreshControl()
        
        self.refreshControl!.rx_controlEvent(.ValueChanged)
            .bindNext(self.refresh)
            .addDisposableTo(disposeBag)
    }
}
