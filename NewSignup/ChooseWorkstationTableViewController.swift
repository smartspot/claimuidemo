//
//  ChooseWorkstationTableViewController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources
import RxCocoa


class WorkstationCell: UITableViewCell {
    @IBOutlet weak var cellTitleLabel: UILabel!
    @IBOutlet weak var cellImageView: UIImageView!
}


class ChooseWorkstationsTableViewController: UITableViewController {
    let sessionProvider = SessionProvider()
    let workoutSetProvider = WorkoutSetWebApiProvider()
    
    var workstations = [Workstation]()
    
    let disposeBag = DisposeBag()
    
    var selection = Variable(Set<Workstation>())
    var sets = Variable([WorkoutSet]())
    
    let nextButton = UIBarButtonItem(title: "Next", style: .Done, target: nil, action: nil)

    var date: NSDate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sessionProvider.login().subscribeNext({session in
            self.workoutSetProvider.unclaimedSetsAtHomeLocationOnDate(session, date: self.date!)
                .subscribeNext({ sets in
                    self.sets.value = sets
                })
                .addDisposableTo(self.disposeBag)
        })
        .addDisposableTo(disposeBag)
        
        self.tableView.rx_itemDeselected.subscribeNext({ path in
            let workstation = self.workstations[path.row]
            
            guard let cell = self.tableView.cellForRowAtIndexPath(path) else {
                return
            }
            
            self.selection.value.remove(workstation)
            cell.accessoryType = .None
        })
        .addDisposableTo(disposeBag)
        
        self.tableView.rx_itemSelected.subscribeNext({ path in
            let workstation = self.workstations[path.row]
            
            guard let cell = self.tableView.cellForRowAtIndexPath(path) else {
                return
            }
            
            self.selection.value.insert(workstation)
            cell.accessoryType = .Checkmark
        })
        .addDisposableTo(disposeBag)
        
        self.navigationItem.rightBarButtonItem = nextButton
        
        
        let transitionPreconditions = [self.selection.asObservable().map({$0.count > 0}),
                                       self.sets.asObservable().map({$0.count > 0})]
        
        transitionPreconditions
            .zip({$0.allEqual(true)})
            .bindTo(nextButton.rx_enabled)
            .addDisposableTo(disposeBag)
        
        nextButton.rx_tap.asObservable()
            .subscribeNext({
            self.transitionToWorkstations()
        }).addDisposableTo(disposeBag)
        
        Observable.just(workstations).bindTo(self.tableView.rx_itemsWithCellIdentifier("WorkstationCell", cellType: WorkstationCell.self)) { (row, value, cell) in
            cell.cellTitleLabel.text = value.name
            cell.cellImageView.setUrl(value.imageUrl)
            }
            .addDisposableTo(self.disposeBag)
    }
    
    func transitionToWorkstations() {
        let controller = MultipleWorkstationClaimController.create(Array(selection.value), allWorkoutSets: sets.value)
        controller.date = self.date
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    func prepare(workstations: [Workstation]) {
        self.workstations = workstations
    }
    
    static func create(workstations: [Workstation]) -> ChooseWorkstationsTableViewController {
        let controller = UIStoryboard(name: "MakeClaim", bundle: nil).instantiateViewControllerWithIdentifier("ChooseWorkstationsTableViewController") as! ChooseWorkstationsTableViewController
        
        controller.prepare(workstations)
        
        return controller
    }
    
    // cell height
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
}