//
//  ChooseDateViewController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/16/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift


class ChooseDateViewController: UIViewController {
    @IBOutlet weak var justNowButton: UIButton!
    @IBOutlet weak var chooseDateButton: UIButton!
    @IBOutlet weak var dateBox: UITextField!
    
    let sessionProvider = SessionProvider()
    let provider = LocationWebApiProvider()
    
    let disposeBag = DisposeBag()
    
    var workstations = [Workstation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sessionProvider.login().subscribeNext({session in
            self.provider.getHomeLocation(session)
                .subscribeNext({ location in
                    self.workstations = location.workstations
                })
                .addDisposableTo(self.disposeBag)
        })
        .addDisposableTo(disposeBag)
        
        justNowButton.rx_tap.subscribeNext({
            let now = NSDate()
            self.transitionToChooseWorkstation(now)
        })
        .addDisposableTo(disposeBag)
        
        chooseDateButton.rx_tap.subscribeNext({
            self.dateBox.becomeFirstResponder()
        })
        .addDisposableTo(disposeBag)
        
        initializeDatePickerAccessories()
    }
    
    func initializeDatePickerAccessories() {
        let toolbar = UIToolbar()
        
        let okButton = UIBarButtonItem(barButtonSystemItem: .Done, target: nil, action: nil)
        let space = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .Cancel, target: nil, action: nil)
        
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 320))
        
        toolbar.barStyle = .Black
        
        toolbar.items = [cancelButton, space, okButton]
        toolbar.sizeToFit()
        
        dateBox.inputView = datePicker
        dateBox.inputAccessoryView = toolbar
        
        cancelButton.rx_tap.subscribeNext({
            self.dateBox.endEditing(true)
        })
        .addDisposableTo(disposeBag)
        
        okButton.rx_tap.subscribeNext({
            self.dateBox.endEditing(true)
            self.transitionToChooseWorkstation(datePicker.date)
        })
        .addDisposableTo(disposeBag)
    }
    
    func transitionToChooseWorkstation(date: NSDate) {
        print("Chose \(date)")
        
        let controller = ChooseWorkstationsTableViewController.create(workstations)
        controller.date = date
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    @IBAction func cancelButtonTapped(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
