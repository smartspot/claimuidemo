//
//  MultipleWorkstationClaimController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/15/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebImage

class MultipleWorkstationClaimController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var workstation: Workstation!
    var workoutSets = [WorkoutSet]()
    
    let disposeBag = DisposeBag()
    
    let claims = Variable<Dictionary<WorkoutSet, WorkoutSetClaimRequest>>(Dictionary<WorkoutSet, WorkoutSetClaimRequest>())
    var date: NSDate?
    
    var workstations = [Workstation]()
    var allWorkoutSets = [WorkoutSet]()
    var workstationIndex = 0
    
    let nextButton = UIBarButtonItem(title: "Next", style: .Done, target: nil, action: nil)
    let doneButton = UIBarButtonItem(barButtonSystemItem: .Done, target: nil, action: nil)
    
    var badgeController: ScrollingBadgeIndicatorController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = VerticalGridFlowLayout(columns: 3, itemHeight: 200.0)
        self.collectionView?.setCollectionViewLayout(layout, animated: false)
        
        self.navigationItem.title = workstation.name
        
        initializeBindings(Observable.just(workoutSets))
        
        if workstationIndex == workstations.count - 1 {
            self.navigationItem.rightBarButtonItem = doneButton
        } else {
            self.navigationItem.rightBarButtonItem = nextButton
        }
        nextButton.rx_tap.subscribeNext(self.transitionNextWorkstation).addDisposableTo(disposeBag)
        doneButton.rx_tap.subscribeNext(self.transitionReviewController).addDisposableTo(disposeBag)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        if let date = date {
            scrollToItem(date)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embed" {
            badgeController = segue.destinationViewController as! ScrollingBadgeIndicatorController
        }
    }
    
    func initializeBindings(workoutSetsObservable: Observable<[WorkoutSet]>) {
        workoutSetsObservable.bindTo(self.collectionView!.rx_itemsWithCellIdentifier(WorkoutSetCell().ReuseIdentifier, cellType: WorkoutSetCell.self)) { (_, value, cell) in
            cell.render(value)
            if self.claims.value[value] != nil {
                cell.showSelected()
            }
            cell.rx_longPressed.subscribeNext(self.showPreviewForCell).addDisposableTo(self.disposeBag)
            }.addDisposableTo(disposeBag)
        
        self.collectionView?.allowsSelection = true
        self.collectionView?.allowsMultipleSelection = true
        
        // handle cell selection
        self.collectionView?
            .rx_itemSelected
            .map(workoutSetCellForItemAtIndexPath)
            .subscribeNext({ cell in
                self.claims.value[cell.workoutSet] = WorkoutSetClaimRequest(workoutSet: cell.workoutSet, user: SessionProvider.sharedSession!.user)
                cell.showSelected()
            })
            .addDisposableTo(disposeBag)
        
        // handle cell deselection
        self.collectionView?
            .rx_itemDeselected
            .map(workoutSetCellForItemAtIndexPath)
            .subscribeNext({ cell in
                self.claims.value.removeValueForKey(cell.workoutSet)
                cell.showDeselected()
            })
            .addDisposableTo(disposeBag)
    }
    
    func showPreviewForCell(cell : WorkoutSetCell) {
        if let image = cell.image.image {
            self.showDetailViewController(ImagePreviewController(image: image), sender: nil)
        }
    }
    
    func workoutSetCellForItemAtIndexPath(path: NSIndexPath) -> WorkoutSetCell {
        return self.collectionView!.cellForItemAtIndexPath(path) as! WorkoutSetCell
    }
    
    func prepare(workstation: Workstation, workoutSets: [WorkoutSet], claims: Dictionary<WorkoutSet, WorkoutSetClaimRequest> = Dictionary<WorkoutSet, WorkoutSetClaimRequest>()) {
        self.workstation = workstation
        self.workoutSets = workoutSets.sort()
        self.claims.value = claims
    }
    
    func prepare(workstations: [Workstation],
                 allWorkoutSets: [WorkoutSet],
                 claims: Dictionary<WorkoutSet, WorkoutSetClaimRequest> = Dictionary<WorkoutSet, WorkoutSetClaimRequest>(),
                 workstationIndex: Int = 0) {
        self.workstations = workstations
        self.allWorkoutSets = allWorkoutSets
        self.workstationIndex = workstationIndex
        
        self.prepare(workstations[workstationIndex], workoutSets: allWorkoutSets.filter({$0.workstationId == workstations[workstationIndex].id}), claims: claims)
    }
    
    private func transitionNextWorkstation() {
        let controller = self.storyboard?.instantiateViewControllerWithIdentifier("ClaimWorkoutSetsCollectionViewController") as! MultipleWorkstationClaimController
        
        controller.prepare(workstations, allWorkoutSets: allWorkoutSets, claims: self.claims.value, workstationIndex: workstationIndex + 1)
        
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    private func transitionReviewController() {
        let controller = ReviewClaimsController.create(Array(self.claims.value.values))
        self.navigationController!.pushViewController(controller, animated: true)
    }
    
    static func create(workstations: [Workstation], allWorkoutSets: [WorkoutSet]) -> MultipleWorkstationClaimController {
        let controller = UIStoryboard(name: "MakeClaim", bundle: nil).instantiateViewControllerWithIdentifier("ClaimWorkoutSetsCollectionViewController") as! MultipleWorkstationClaimController
        
        controller.prepare(workstations, allWorkoutSets: allWorkoutSets)
        
        return controller
    }
    
    @IBOutlet weak var containerView: UIView!
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
//        print(scrollView.contentOffset.y)
        var label = ""
        let count = collectionView.visibleCells().count
        if count >= 1 {
            let yOrigin = max(scrollView.contentOffset.y + 64 + containerView.frame.origin.y, 0)
            print(yOrigin)
            let topLeftPoint = CGPointMake(0, yOrigin)
            if let indexPath = collectionView.indexPathForItemAtPoint(topLeftPoint) {
                let workoutSet = workoutSets[indexPath.row]
                label = "\(workoutSet.startTime.components().hour):00"
            }
        }
        
//        let inset = scrollView.contentInset
//        print(inset)
        
//        let inset = scrollView
//        print(inset.y)
        
//        let offset = scrollView.contentOffset
//        let computedTop = offset.y + inset.top
//        print(computedTop)
        
//        let computedBottom = scrollView.contentSize.height + inset.top - CGRectGetHeight(scrollView.frame)
//        print(computedBottom)
        
//        let percentage = computedTop / computedBottom
        badgeController.showBadge(true, withTitle: label)
    }
    
    func scrollToItem(timestamp: NSDate) -> Void {
        if let index = self.indexForWorkoutSet(timestamp) {
            let indexPath = NSIndexPath(forItem: index, inSection: 0)
            self.collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: UICollectionViewScrollPosition.Top, animated: false)
        }
    }
    
    private func indexForWorkoutSet(timestamp: NSDate) -> Int? {
        let hour = timestamp.components().hour
        
        for set in self.workoutSets {
            if set.startTime.components.hour == hour {
                if let index = self.workoutSets.indexOf(set) {
                    return Int(index)
                }
            }
        }
        
        return nil
    }
}
