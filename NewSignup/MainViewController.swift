//
//  ViewController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 4/23/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import Eureka


@IBDesignable class HexagonBackgroundView : UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        initBackground()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initBackground()
    }
    
    override func prepareForInterfaceBuilder() {
        initBackground()
    }
    
    func initBackground() {
        let bundle = NSBundle(forClass: self.dynamicType)
        let image = UIImage(named: "HexagonPattern", inBundle: bundle, compatibleWithTraitCollection: self.traitCollection)!

        self.backgroundColor = UIColor.init(patternImage: image)
    }
}


class CheckInViewController : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

class TodayViewController : UIViewController {
    @IBOutlet weak var tableViewController: UITableView!
    let strings = ["Hello", "World", "Seriously"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewController.delegate = self
        tableViewController.dataSource = self
    }
}


class CollectionTableCell : UITableViewCell, UICollectionViewDataSource {
    @IBOutlet weak var collectionView: UICollectionView!
    let strings = (1..<100).map({"\($0)"})
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func initializeCollectionView() {
        collectionView.dataSource = self
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return strings.count
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        
        let label = UILabel()
        label.text = strings[indexPath.row]
        label.textColor = UIColor.blueColor()
        cell.backgroundView = label
        return cell
    }
}

extension TodayViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return strings.count
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterViewWithIdentifier("Header")
        
        cell?.backgroundColor = UIColor.clearColor()
        
        return cell
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Row", forIndexPath: indexPath) as! CollectionTableCell
        
        cell.textLabel?.text = strings[indexPath.row]
        cell.backgroundColor = UIColor.clearColor()
        
        cell.initializeCollectionView()
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150.0
    }
}

extension TodayViewController : UITableViewDelegate {
}


class MainViewController : FormViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        form +++ Section("Account")
            <<< TextRow(){
                $0.placeholder = "Jon"
                $0.title = "First Name"
            }
            <<< TextRow(){
                $0.placeholder = "Snow"
                $0.title = "Last Name"
            }
            <<< ImageRow(){
                $0.title = "Face Picture"
            }
        
        let button = ButtonRow("Submit")
        button.title = "Submit"
        button.onCellSelection({(cell, row) in
            self.takePhoto(button)
        })
        
        form +++ Section("Stuff")
            <<< button
        
    }
    
    @IBAction func takePhoto(sender: AnyObject) {
        if !UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera){
            return
        }
        
        let imagePicker = UIImagePickerController()
        //imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.Camera;
        
        //Create camera overlay
        let pickerFrame = CGRectMake(0, UIApplication.sharedApplication().statusBarFrame.size.height, imagePicker.view.bounds.width, imagePicker.view.bounds.height - imagePicker.navigationBar.bounds.size.height - imagePicker.toolbar.bounds.size.height)
        let squareFrame = CGRectMake(pickerFrame.width/2 - 200/2, pickerFrame.height/2 - 200/2, 200, 200)
        UIGraphicsBeginImageContext(pickerFrame.size)
        
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextAddRect(context, CGContextGetClipBoundingBox(context))
        CGContextMoveToPoint(context, squareFrame.origin.x, squareFrame.origin.y)
        CGContextAddLineToPoint(context, squareFrame.origin.x + squareFrame.width, squareFrame.origin.y)
        CGContextAddLineToPoint(context, squareFrame.origin.x + squareFrame.width, squareFrame.origin.y + squareFrame.size.height)
        CGContextAddLineToPoint(context, squareFrame.origin.x, squareFrame.origin.y + squareFrame.size.height)
        CGContextAddLineToPoint(context, squareFrame.origin.x, squareFrame.origin.y)
        CGContextEOClip(context)
        CGContextMoveToPoint(context, pickerFrame.origin.x, pickerFrame.origin.y)
        CGContextSetRGBFillColor(context, 0, 0, 0, 0.2)
        CGContextFillRect(context, pickerFrame)
        CGContextRestoreGState(context)
        
        let overlayImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        let overlayView = UIImageView(frame: pickerFrame)
        overlayView.image = overlayImage
        imagePicker.cameraOverlayView = overlayView
        self.presentViewController(imagePicker, animated: true, completion: nil)
    }
}