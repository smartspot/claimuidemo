//
//  UserSession.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/13/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation


class UserSession {
    let user: User
    
    init(user: User) {
        self.user = user
    }
}