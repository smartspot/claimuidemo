//
//  VerticalCollectionViewFlow.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/15/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit


class VerticalGridFlowLayout : UICollectionViewFlowLayout {
    var columns = 3
    var itemHeight = CGFloat(50)
    
    init(columns : Int, itemHeight: CGFloat) {
        super.init()
        
        self.columns = columns
        self.itemHeight = itemHeight
        
        setupLayout()
    }
    
    override init() {
        super.init()
        setupLayout()
    }
    
    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = CGFloat(columns)
            
            let itemWidth = (CGRectGetWidth(self.collectionView!.frame) - (numberOfColumns - 1)) / numberOfColumns
            
            return CGSizeMake(itemWidth, itemHeight)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .Vertical
    }
}