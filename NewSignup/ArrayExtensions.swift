//
//  ArrayExtensions.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation



extension Array where Element : Equatable {
    func allEqual(value: Element) -> Bool {
        return self.filter({$0 == value}).count == self.count
    }
    
    mutating func removeValue(value: Element) {
        if let index = self.indexOf(value) {
            self.removeAtIndex(index)
        }
    }
}

func groupBy<Element: Equatable, Key : Hashable>(collection: Array<Element>, keyProperty: (Element) -> Key) -> Dictionary<Key, Set<Element>> {
    var result = Dictionary<Key, Set<Element>>()
    
    for element in collection {
        let key = keyProperty(element)
        if case nil = result[key]?.insert(element) {
            result[key] = Set([element])
        }
    }
    
    return result
}