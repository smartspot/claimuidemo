//
//  WorkoutSetClaimProvider.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/16/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift


protocol WorkoutSetClaimProvider {
    func makeClaims(claimRequests: [WorkoutSetClaimRequest]) -> Observable<[WorkoutSetClaim]>
    func getClaims(userSession: UserSession) -> Observable<[WorkoutSetClaim]>
}


class WorkoutSetClaimWebApiProvider : WebApiProvider, WorkoutSetClaimProvider {
    func makeClaims(claimRequests: [WorkoutSetClaimRequest]) -> Observable<[WorkoutSetClaim]> {
        let url = NSURL(string:"\(baseUrl)/workout_sets/claim")!
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "PUT"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let claimWorkoutSetIds = ["ids": claimRequests.map({$0.workoutSet.id.UUIDString})]
        
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(claimWorkoutSetIds, options: [])
        } catch {
            return Observable.error(NSError(domain: "world", code:200, userInfo:nil))
        }
        
        return WebApiProvider.Manager
            .rx_request(request)
            .flatMap{$0.validate(statusCode: 200..<300).rx_JSON()}
            .map{JSON($0)}
            .map{$0.arrayValue.map({json in WorkoutSetClaim(json:json)!})}
    }
    
    func getClaims(userSession: UserSession) -> Observable<[WorkoutSetClaim]> {
        let url = "\(baseUrl)/me/claims/pending"
        
        return WebApiProvider.Manager
            .rx_JSON(.GET, url)
            .map{json in JSON(json) }
            .map{json in json.arrayValue.flatMap({WorkoutSetClaim(json: $0)!})}
    }
}