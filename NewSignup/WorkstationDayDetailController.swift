//
//  WorkstationDayDetailController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/18/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources


class WorkoutSetDetailCell : UITableViewCell {
    @IBOutlet weak var claimView: WorkoutSetClaimView!
    
    @IBOutlet weak var setLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    var claim: WorkoutSetClaim!
    
    func render(index: Int, claim: WorkoutSetClaim) {
        self.claim = claim
        
        claimView.claim = claim
        setLabel.text = "Set \(index + 1)"
        timeLabel.text = "\(claim.workoutSet.startTime.timeString)"
    }
}


class WorkstationDayDetailController: UITableViewController {
    var claims: [WorkoutSetClaim]!
    
    let disposeBag = DisposeBag()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Squat" // TODO: Use the name of the workstation of the set.
        
        Observable.just(self.claims)
            .bindTo(self.tableView.rx_itemsWithCellIdentifier("WorkoutSetDetailCell", cellType: WorkoutSetDetailCell.self)) { (row, value, cell) in
                cell.render(row, claim: value)
            }
            .addDisposableTo(disposeBag)
    }
    
    func prepare(claims: [WorkoutSetClaim]) {
        self.claims = claims.sort({(l, r) in l.workoutSet.startTime < r.workoutSet.startTime})
    }
    
    static func create(claims: [WorkoutSetClaim]) -> WorkstationDayDetailController {
        let controller = UIStoryboard(name: "Feed", bundle: nil).instantiateViewControllerWithIdentifier("WorkstationDayDetailController") as! WorkstationDayDetailController
        
        controller.prepare(claims)
        
        return controller
    }
}