//
//  Renders.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit

protocol Renders {
    associatedtype Model
    
    func render(model: Model)
}