//
//  NSDateExtensions.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation


extension NSDate {
    var iso8601DateString : String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        return formatter.stringFromDate(self)
    }
    
    var timeString : String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "h:mm a"
        
        return formatter.stringFromDate(self)
    }
}