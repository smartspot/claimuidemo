//
//  PolygonButton.swift
//  NewSignup
//
//  Created by Joshua Augustin on 4/24/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit


struct CornerPoint {
    let centerPoint : CGPoint
    let startAngle : CGFloat
    let endAngle : CGFloat
    let cornerRadius: CGFloat
    
    func draw(context: CGContext?) {
        CGContextAddArc(context, centerPoint.x, centerPoint.y, cornerRadius, endAngle, startAngle, YESEXPR)
    }
}

func roundedCornerWithLinesFrom(from: CGPoint, via: CGPoint, to: CGPoint, withRadius radius:CGFloat) -> CornerPoint {
    let fromAngle = CGFloat(atan2(via.y - from.y, via.x - from.x));
    let toAngle = CGFloat(atan2(to.y  - via.y, to.x  - via.x));
    
    let fromOffset = CGVectorMake(-sin(fromAngle) * radius, cos(fromAngle) * radius);
    let toOffset = CGVectorMake(-sin(toAngle) * radius, cos(toAngle) * radius);
    
    
    let x1 = from.x + fromOffset.dx
    let y1 = from.y + fromOffset.dy
    
    let x2 = via.x  + fromOffset.dx
    let y2 = via.y  + fromOffset.dy
    
    let x3 = via.x  + toOffset.dx
    let y3 = via.y  + toOffset.dy
    
    let x4 = to.x   + toOffset.dx
    let y4 = to.y   + toOffset.dy
    
    let intersectionX = ((x1*y2-y1*x2)*(x3-x4) - (x1-x2)*(x3*y4-y3*x4)) / ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
    let intersectionY = ((x1*y2-y1*x2)*(y3-y4) - (y1-y2)*(x3*y4-y3*x4)) / ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4));
    
    let intersection = CGPointMake(intersectionX, intersectionY);
    
    return CornerPoint(centerPoint: intersection, startAngle: fromAngle - CGFloat(M_PI_2), endAngle: toAngle - CGFloat(M_PI_2), cornerRadius: radius)
}


@IBDesignable class PolygonButton: UIButton {
    @IBInspectable var sides : Int = 6
    @IBInspectable var borderColor : UIColor = UIColor.greenColor()
    @IBInspectable var fillColor : UIColor = UIColor.clearColor()
    @IBInspectable var borderWidth: CGFloat = 1.0
    @IBInspectable var cornerRadius: CGFloat = 0.0
    
    func radiusOfRect(rect: CGRect) -> CGFloat {
        return min(rect.width / 2, rect.height / 2) - borderWidth
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initHandlers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initHandlers()
    }
    
    func initHandlers() {
        self.addTarget(self, action: #selector(buttonDepressed), forControlEvents: .TouchDown)
        self.addTarget(self, action: #selector(buttonUnpressed), forControlEvents: .TouchUpInside)
        self.addTarget(self, action: #selector(buttonUnpressed), forControlEvents: .TouchUpOutside)
    }
    
    func buttonDepressed(sender: AnyObject) {
        self.fillColor = self.borderColor.colorWithAlphaComponent(0.5)
        self.setNeedsDisplay()
    }
    
    func buttonUnpressed(sender: AnyObject) {
        self.fillColor = UIColor.clearColor()
        self.setNeedsDisplay()
    }

    override func drawRect(rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        
        CGContextSaveGState(context);
        
        let borderColor = self.borderColor.CGColor
        
        CGContextBeginPath(context)
        
        CGContextSetStrokeColorWithColor(context, borderColor)
        CGContextSetFillColorWithColor(context, fillColor.CGColor)
        CGContextSetLineWidth(context, borderWidth)
        
        let topPoint = pointAtIndex(rect, index: 0)
        let nextPoint = pointAtIndex(rect, index: 1)
        
        CGContextMoveToPoint(context, (topPoint.x + nextPoint.x) / 2, (topPoint.y + nextPoint.y) / 2)
        
        for i in 0..<sides {
            let last = pointAtIndex(rect, index: -i - 1)
            let point = pointAtIndex(rect, index: -i)
            let next = pointAtIndex(rect, index: -i + 1)
            
            let corner = roundedCornerWithLinesFrom(last, via: point, to: next, withRadius: cornerRadius)
            corner.draw(context)
        }
        
        CGContextClosePath(context)
        CGContextDrawPath(context, .FillStroke)
        CGContextRestoreGState(context)
    }
        
    func pointAtIndex(rect: CGRect, index: Int) -> CGPoint {
        let center = CGPointMake(rect.width / 2, rect.height / 2)
        let radius = radiusOfRect(rect)
        
        let x = radius * CGFloat(sinf(Float(index) * Float(2.0) * Float(M_PI) / Float(sides)));
        let y = -radius * CGFloat(cosf(Float(index) * Float(2.0) * Float(M_PI) / Float(sides)));
        
        return CGPointMake(center.x + x, center.y + y);
    }
}
