//
//  UIImageViewExtensions.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/15/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit


extension UIImageView {
    func setUrl(url: NSURL?, fadeIn: Bool = true) {
        guard let url = url
            else { return }
        
        self.sd_setImageWithURL(url) { (image, error, cacheType, url) in
            if image != nil && cacheType == .None {
                dispatch_async(dispatch_get_main_queue(), { 
                    self.alpha = 0
                    UIView.animateWithDuration(1.0) {
                        self.alpha = 1.0
                    }
                })
            }
        }
    }
}