//
//  WorkoutSetClaimRequest.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/15/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation
import SwiftyJSON


public class WorkoutSetClaimRequest : Model, Equatable, Hashable {
    let workoutSet: WorkoutSet
    let user: User
    
    init(workoutSet: WorkoutSet, user: User) {
        self.workoutSet = workoutSet
        self.user = user
    }
    
    public var hashValue: Int {
        return workoutSet.hashValue &* user.hashValue // overflow multiplication
    }
}

public func ==(lhs: WorkoutSetClaimRequest, rhs: WorkoutSetClaimRequest) -> Bool {
    return lhs.workoutSet == rhs.workoutSet && lhs.user == rhs.user
}