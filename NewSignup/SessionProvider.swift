//
//  SessionProvider.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/13/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire
import Alamofire
import SwiftyJSON


class SessionProvider : WebApiProvider {
    static var sharedSession : UserSession?
    
    func login(email: String =  "themetareality@gmail.com", password: String = "Josh Is God") -> Observable<UserSession> {
        let url = NSURL(string:"\(baseUrl)/login")!
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        let data = ["email": email, "password": password]
        do {
            request.HTTPBody = try NSJSONSerialization.dataWithJSONObject(data, options: [])
        } catch {
            return Observable.error(NSError(domain: "world", code:200, userInfo:nil))
        }
        
        return WebApiProvider.Manager
            .rx_request(request)
            .flatMap{
                $0.validate(statusCode: 200..<300)
                  .rx_data()
                  .map{data in JSON(data: data)}
                  .map{json in return UserSession(user: User(json: json)!)}
            }
            .doOnNext{ session in
                SessionProvider.sharedSession = session
                print("Logged in.")
            }
            .doOnError{ errors in
                print("Failed to log in.")
            }
    }
}