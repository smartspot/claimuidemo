//
//  ScrollingBadgeIndicatorController.swift
//  NewSignup
//
//  Created by Robot MBP on 5/25/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit

class ScrollingBadgeIndicatorController: UIViewController {
    @IBOutlet weak var badge: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    
    var dismissTimer: NSTimer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        badge.layer.cornerRadius = 5.0
        badge.clipsToBounds = true
        badge.alpha = 0
        view.backgroundColor = UIColor.clearColor()
    }
    
    func showBadge(show: Bool, withTitle title: String?) {
        if show == true {
            UIView.animateKeyframesWithDuration(0.2, delay: 0, options: UIViewKeyframeAnimationOptions.BeginFromCurrentState, animations: { 
                self.badge.alpha = 1.0
                }, completion: nil)

            if let dismissTimer = dismissTimer {
                dismissTimer.fireDate = NSDate().dateByAddingTimeInterval(1.0)
            } else {
                self.dismissTimer = NSTimer(fireDate: NSDate().dateByAddingTimeInterval(1.0), interval: 0, target: self, selector: #selector(ScrollingBadgeIndicatorController.dismissBadge), userInfo: nil, repeats: false)
                NSRunLoop.mainRunLoop().addTimer(self.dismissTimer!, forMode: NSDefaultRunLoopMode)
            }
        }
        
        badgeLabel.text = title
    }
    
    func dismissBadge() {
        UIView.animateKeyframesWithDuration(0.4, delay: 0, options: UIViewKeyframeAnimationOptions.AllowUserInteraction, animations: {
            self.badge.alpha = 0
            }, completion: nil)
        dismissTimer?.invalidate()
        dismissTimer = nil
    }
}
