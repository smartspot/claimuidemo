//
//  FeedNavigationController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/16/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class FeedNavigationController: UITabBarController {
    let button = UIButton()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let buttonSize : CGFloat = 60.0
        let buttonColor = UIColor.init(red: 0.2, green: 0.5, blue: 0.9, alpha: 1.0)
        
        button.frame = CGRect(center: CGPoint(x:self.tabBar.frame.midX, y: 20), size: CGSize(width: buttonSize, height: buttonSize))
        button.setTitle("+", forState: .Normal)
        
        button.backgroundColor = buttonColor
        button.imageEdgeInsets = UIEdgeInsetsMake(20, 20, 20, 20)
        button.setImage(UIImage(named: "plus"), forState: .Normal)
        
        button.tintColor = UIColor.redColor()
        
        button.layer.cornerRadius = buttonSize / 2.0
        
        self.tabBar.addSubview(button)
        self.tabBar.bringSubviewToFront(button)
        
        button.rx_tap.subscribeNext({
            self.performSegueWithIdentifier("showMakeClaimFlow", sender: nil)
        })
        .addDisposableTo(disposeBag)
    }
    
}
