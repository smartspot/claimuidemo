//
//  AppDelegate.swift
//  NewSignup
//
//  Created by Joshua Augustin on 4/23/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    let disposeBag = DisposeBag()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        SessionProvider().login()
            .subscribeNext({session in
                print("Logged in")
            })
            .addDisposableTo(disposeBag)
        return true
    }
}

