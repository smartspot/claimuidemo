//
//  WorkoutSetProvider.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation
import SwiftyJSON
import RxSwift


protocol WorkoutSetProvider {
    func unclaimedSetsAtHomeLocationOnDate(session: UserSession, date: NSDate) -> Observable<[WorkoutSet]>
}


class WorkoutSetWebApiProvider : WebApiProvider, WorkoutSetProvider {
    func unclaimedSetsAtHomeLocationOnDate(session: UserSession, date: NSDate) -> Observable<[WorkoutSet]> {
        print("date: \(date)")
        let date_ = NSDate.init(ISO8601String: "2016-04-31")!
        let url = "\(baseUrl)/me/home_location/workout_sets/\(date_.iso8601DateString)/unassigned"
        
        return WebApiProvider.Manager
            .rx_JSON(.GET, url)
            .map{json in JSON(json)}
            .map{json in json.arrayValue.flatMap(WorkoutSet.init)}
    }
}