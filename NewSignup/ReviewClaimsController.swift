//
//  ReviewClaimsController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/15/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxSwift


class WorkoutSetClaimRequestCell : WorkoutSetCell {
    var claimRequest : WorkoutSetClaimRequest!
    
    override var ReuseIdentifier : String {
        return "workoutSetClaimRequestCell"
    }
    
    func render(claimRequest: WorkoutSetClaimRequest) {
        self.claimRequest = claimRequest
        render(claimRequest.workoutSet)
    }
}


class ReviewClaimsController : UICollectionViewController {
    var claims = [WorkoutSetClaimRequest]()
    var finalClaims = Set<WorkoutSetClaimRequest>()
    
    let disposeBag = DisposeBag()
    let submitButton = UIBarButtonItem(title: "Submit", style: .Done, target: nil, action: nil)
    let workoutSetClaimProvider = WorkoutSetClaimWebApiProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = VerticalGridFlowLayout(columns: 3, itemHeight: 200.0)
        
        self.navigationItem.title = "Review Claims"
        self.navigationItem.rightBarButtonItem = submitButton
        
        self.collectionView?.setCollectionViewLayout(layout, animated: false)
        
        self.collectionView?.allowsSelection = true
        self.collectionView?.allowsMultipleSelection = true
        
        self.collectionView!.rx_itemSelected
            .map(workoutSetClaimRequestCellForItemAtIndexPath)
            .subscribeNext({cell in
                cell.showSelected()
                self.finalClaims.insert(cell.claimRequest)
            })
            .addDisposableTo(disposeBag)
        
        self.collectionView!.rx_itemDeselected
            .map(workoutSetClaimRequestCellForItemAtIndexPath)
            .subscribeNext({cell in
                cell.showDeselected()
                self.finalClaims.remove(cell.claimRequest)
            })
            .addDisposableTo(disposeBag)
        
        Observable.just(claims)
            .bindTo(self.collectionView!.rx_itemsWithCellIdentifier(WorkoutSetClaimRequestCell().ReuseIdentifier, cellType: WorkoutSetClaimRequestCell.self)) { (row, claim, cell) in
            cell.render(claim)
            cell.selected = true
            self.collectionView?.selectItemAtIndexPath(NSIndexPath(forRow: row, inSection: 0), animated: false, scrollPosition: .Right)
            cell.showSelected()
        }.addDisposableTo(disposeBag)
        
        submitButton.rx_tap
            .subscribeNext(self.makeClaims)
            .addDisposableTo(disposeBag)
    }
    
    func workoutSetClaimRequestCellForItemAtIndexPath(path: NSIndexPath) -> WorkoutSetClaimRequestCell {
        return self.collectionView!.cellForItemAtIndexPath(path) as! WorkoutSetClaimRequestCell
    }
    
    func makeClaims() {
        workoutSetClaimProvider.makeClaims(Array(finalClaims))
            .subscribeNext({ claims in
                self.dismissViewControllerAnimated(true, completion: nil)
            })
            .addDisposableTo(disposeBag)
    }
    
    func prepare(claims: [WorkoutSetClaimRequest]) {
        self.claims = claims
        self.finalClaims = Set<WorkoutSetClaimRequest>(claims)
    }
    
    static func create(claims: [WorkoutSetClaimRequest]) -> ReviewClaimsController {
        let controller = UIStoryboard(name: "MakeClaim", bundle: nil).instantiateViewControllerWithIdentifier("ReviewClaimsController") as! ReviewClaimsController
        
        controller.prepare(claims)
        
        return controller
    }
}