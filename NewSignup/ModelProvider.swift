//
//  ModelProvider.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation


protocol ModelProvider {
    associatedtype ModelType
    
    func get(id: IdModel.IdType) -> ModelType
}