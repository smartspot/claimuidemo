//
//  ClaimWorkoutSetsCollectionViewController.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebImage

class WorkoutSetCell : UICollectionViewCell {
    static let reuseIdentifier = "workoutSetCell"
    var workoutSet: WorkoutSet!
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    var longPressGestureRecognizer : UILongPressGestureRecognizer!
    
    func render(workoutSet: WorkoutSet) {
        self.workoutSet = workoutSet
        label.text = self.workoutSet.startTime.timeString
        
        image.setUrl(self.workoutSet.thumbnail.url)
        
        if self.gestureRecognizers == nil || self.gestureRecognizers?.count == 1 {
            longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(showDetail))
            self.contentView.addGestureRecognizer(longPressGestureRecognizer)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.showDeselected()
    }
    
    var ReuseIdentifier : String {
        return "workoutSetCell"
    }
    
    func showSelected() {
        let color = UIColor.greenColor()
        
        self.layer.borderWidth = 5.0
        self.layer.borderColor = color.CGColor
        
        self.label.textColor = color
    }
    
    func showDeselected() {
        self.layer.borderWidth = 0.0
        self.label.textColor = UIColor.whiteColor()
    }
    
    func showDetail() {
        print("Showing detail for \(workoutSet.id)")
    }
    
    var rx_longPressed : Observable<WorkoutSetCell> {
        let me = self
        return longPressGestureRecognizer.rx_event.filter{ recognizer in
            recognizer.state == .Began
        }.map({_ in me})
    }
}


class ClaimWorkoutSetsCollectionViewController: UICollectionViewController {
    var workstation: Workstation!
    var workoutSets = [WorkoutSet]()
    
    let disposeBag = DisposeBag()
    
    let claims = Variable<Dictionary<WorkoutSet, WorkoutSetClaimRequest>>(Dictionary<WorkoutSet, WorkoutSetClaimRequest>())

    override func viewDidLoad() {
        super.viewDidLoad()

        let layout = VerticalGridFlowLayout(columns: 3, itemHeight: 200.0)
        self.collectionView?.setCollectionViewLayout(layout, animated: false)
        
        self.navigationItem.title = workstation.name
        
        initializeBindings(Observable.just(workoutSets))
    }
    
    func initializeBindings(workoutSetsObservable: Observable<[WorkoutSet]>) {
        workoutSetsObservable.bindTo(self.collectionView!.rx_itemsWithCellIdentifier(WorkoutSetCell().ReuseIdentifier, cellType: WorkoutSetCell.self)) { (_, value, cell) in
            cell.render(value)
            cell.rx_longPressed.subscribeNext(self.showPreviewForCell).addDisposableTo(self.disposeBag)
        }.addDisposableTo(disposeBag)
        
        self.collectionView?.allowsSelection = true
        self.collectionView?.allowsMultipleSelection = true
        
        self.collectionView?
            .rx_itemSelected
            .map(workoutSetCellForItemAtIndexPath)
            .subscribeNext({ cell in
                self.claims.value[cell.workoutSet] = WorkoutSetClaimRequest(workoutSet: cell.workoutSet, user: SessionProvider.sharedSession!.user)
                cell.showSelected()
            })
            .addDisposableTo(disposeBag)
        
        self.collectionView?
            .rx_itemDeselected
            .map(workoutSetCellForItemAtIndexPath)
            .subscribeNext({ cell in
                self.claims.value.removeValueForKey(cell.workoutSet)
                cell.showDeselected()
            })
            .addDisposableTo(disposeBag)
    }
    
    func showPreviewForCell(cell : WorkoutSetCell) {
        if let image = cell.image.image {
            self.showDetailViewController(ImagePreviewController(image: image), sender: nil)
        }
    }
    
    func workoutSetCellForItemAtIndexPath(path: NSIndexPath) -> WorkoutSetCell {
        return self.collectionView!.cellForItemAtIndexPath(path) as! WorkoutSetCell
    }
    
    func prepare(workstation: Workstation, workoutSets: [WorkoutSet], claims: Dictionary<WorkoutSet, WorkoutSetClaimRequest> = Dictionary<WorkoutSet, WorkoutSetClaimRequest>()) {
        self.workstation = workstation
        self.workoutSets = workoutSets.sort()
        self.claims.value = claims
    }
}