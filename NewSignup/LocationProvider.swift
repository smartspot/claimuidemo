//
//  LocationProvider.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/14/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON


protocol LocationProvider : ModelProvider {
    func getHomeLocation(session: UserSession) -> Observable<Location>
}

class LocationWebApiProvider : WebApiProvider, LocationProvider {
    func getHomeLocation(session: UserSession) -> Observable<Location> {
        let url = "\(baseUrl)/me/home_location"
        
        return WebApiProvider.Manager
            .rx_JSON(.GET, url)
            .map{json in JSON(json)}
            .doOnNext({json in
                print(json)
            })
            .map{json in Location(json:json)!}
    }
    
    func get(id: IdModel.IdType) -> Observable<Location> {
        return Observable.never()
    }
}