//
//  WorkoutSetClaimView.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/17/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import UIKit


class WorkoutSetClaimView: UIView {
    let imageView = UIImageView(image: nil)
    let lockView = UIImageView(image: UIImage(named: "lock"))
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        imageView.contentMode = .ScaleAspectFill
        imageView.autoresizingMask = .FlexibleWidth
        imageView.frame = self.frame
        self.addSubview(imageView)
        
        lockView.contentMode = .ScaleAspectFit
        lockView.autoresizingMask = .FlexibleWidth
        lockView.frame = lockViewFrame
        self.addSubview(lockView)
        
        self.backgroundColor = UIColor.whiteColor()
    }
    
    var lockViewFrame: CGRect {
        return CGRect(center: frame.center, size: CGSize(width: 50, height: 50))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.frame = self.frame
        lockView.frame = lockViewFrame
    }
    
    var claim : WorkoutSetClaim? {
        didSet {
            if let claim = claim {
                self.imageView.setUrl(claim.workoutSet.thumbnail.url)
                
                if claim.status == .Pending {
                    self.imageView.alpha = 0.5
                    self.lockView.alpha = 0.7
                }
                
                lockView.hidden = claim.status != .Pending
            }
        }
    }
}