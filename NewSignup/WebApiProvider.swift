//
//  WebApiProvider.swift
//  NewSignup
//
//  Created by Joshua Augustin on 5/13/16.
//  Copyright © 2016 Joshua Augustin. All rights reserved.
//

import Foundation
import Alamofire


class WebApiProvider {
    let baseUrl = "http://ec2-52-53-215-249.us-west-1.compute.amazonaws.com/api/v1"

    private static var _Manager : Alamofire.Manager?
    static var Manager : Alamofire.Manager! {
        if _Manager == nil {
            let configs = NSURLSessionConfiguration.defaultSessionConfiguration()
            let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage()
            
            configs.HTTPCookieStorage = cookies
            configs.HTTPCookieAcceptPolicy = NSHTTPCookieAcceptPolicy.Always
            _Manager = Alamofire.Manager(configuration: configs)
        }
        
        return _Manager!
    }
}